﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus.pro.prototype.Entities
{
    public class MilitaryAircraft : Aircraft, IMyCloneable<MilitaryAircraft>, ICloneable
    {
        public bool IsAtack { get; set; }

        public MilitaryAircraft()
        {

        }

        public MilitaryAircraft(Aircraft aircraft, bool isAtack)
        {
            Weight = aircraft.Weight;

            Speed = aircraft.Speed;

            High = aircraft.High;

            Length = aircraft.Length;

            Width = aircraft.Width;

            CapacityIfPeople = aircraft.CapacityIfPeople;

            IsAtack = isAtack;
        }
        public MilitaryAircraft(double weight, int speed, double high, double length, double width, int capacityIfPeople,bool isAtack)
        {
            Weight = weight;

            Speed = speed;

            High = high;

            Length = length;

            Width = width;

            CapacityIfPeople = capacityIfPeople;

            IsAtack = isAtack;
        }

        public object Clone()
        {
            return Copy();
        }

        public new MilitaryAircraft Copy()
        {
            return new MilitaryAircraft(Weight, Speed, High, Length, Width, CapacityIfPeople, IsAtack);
        }

        public override string ToString()
        {
            return $"Weight:\t{Weight},\n" +
                $"Speed:\t{Speed},\n" +
                $"High:\t{High},\n" +
                $"Length:\t{Length},\n" +
                $"Width:\t{Width},\n" +
                $"IsAtack:\t{IsAtack},\n" +
                $"CapacityIfPeople:\t{CapacityIfPeople}.";
        }
    }
}
