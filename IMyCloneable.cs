﻿using System;
using System.Collections.Generic;
using System.Text;

namespace otus.pro.prototype
{
    internal interface IMyCloneable<T> where T : class
    {
        T Copy();
    }

}
