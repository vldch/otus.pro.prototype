﻿using System;

namespace otus.pro.prototype.Entities
{
    public class Aircraft : Transport, IMyCloneable<Aircraft>, ICloneable
    {
        public int CapacityIfPeople { get; set; }

        public Aircraft()
        {

        }

        public Aircraft(Transport transport, int capacityIfPeople)
        {
            Weight = transport.Weight;

            Speed = transport.Speed;
            
            High = transport.High;
            
            Length = transport.Length;
            
            Width = transport.Width;

            CapacityIfPeople = capacityIfPeople;
        }

        public Aircraft(double weight, int speed, double high, double length, double width, int capacityIfPeople) 
        {
            Weight = weight;

            Speed = speed;

            High = high;

            Length = length;

            Width = width;

            CapacityIfPeople = capacityIfPeople;
        }

        public object Clone()
        {
            return Copy();
        }

        public Aircraft Copy()
        {
            return new Aircraft(Weight, Speed, High, Length, Width, CapacityIfPeople);
        }

        public override string ToString()
        {
            return $"Weight:\t{Weight},\n" +
                $"Speed:\t{Speed},\n" +
                $"High:\t{High},\n" +
                $"Length:\t{Length},\n" +
                $"Width:\t{Width},\n" +
                $"CapacityIfPeople:\t{CapacityIfPeople}.";
        }

    }
}
