﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using otus.pro.prototype.Entities;

namespace otus.pro.prototype.test
{
    [TestClass]
    public class TransportTest
    {
        [TestMethod]
        public void TestClone()
        {
            var transport = new Transport(50, 850, 15, 80, 55);
            var clone = (Transport)transport.Clone();
            Assert.AreEqual(transport.Weight, clone.Weight);
            Assert.AreEqual(transport.Speed, clone.Speed);
            Assert.AreEqual(transport.High, clone.High);
            Assert.AreEqual(transport.Length, clone.Length);
            Assert.AreEqual(transport.Width, clone.Width);
            Assert.AreEqual(transport.ToString(), clone.ToString());
        }

        [TestMethod]
        public void TestCopy()
        {
            var transport = new Transport(50, 850, 15, 80, 55);
            var copy = transport.Copy();
            Assert.AreEqual(transport.Weight, copy.Weight);
            Assert.AreEqual(transport.Speed, copy.Speed);
            Assert.AreEqual(transport.High, copy.High);
            Assert.AreEqual(transport.Length, copy.Length);
            Assert.AreEqual(transport.Width, copy.Width);
            Assert.AreEqual(transport.ToString(), copy.ToString());
        }
    }
}
