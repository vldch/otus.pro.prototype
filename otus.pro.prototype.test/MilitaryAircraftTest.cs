﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using otus.pro.prototype.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus.pro.prototype.test
{
    [TestClass]
    public class MilitaryAircraftTest
    {
        [TestMethod]
        public void TestClone()
        {
            var militaryAircraft = new MilitaryAircraft(50, 850, 15, 80, 55,250,true);
            var clone = (MilitaryAircraft)militaryAircraft.Clone();
            Assert.AreEqual(militaryAircraft.Weight, clone.Weight);
            Assert.AreEqual(militaryAircraft.Speed, clone.Speed);
            Assert.AreEqual(militaryAircraft.High, clone.High);
            Assert.AreEqual(militaryAircraft.Length, clone.Length);
            Assert.AreEqual(militaryAircraft.Width, clone.Width);
            Assert.AreEqual(militaryAircraft.CapacityIfPeople, clone.CapacityIfPeople);
            Assert.AreEqual(militaryAircraft.IsAtack, clone.IsAtack);
            Assert.AreEqual(militaryAircraft.ToString(), clone.ToString());
        }

        [TestMethod]
        public void TestCopy()
        {
            var militaryAircraft = new MilitaryAircraft(50, 850, 15, 80, 55, 250, true);
            var copy = militaryAircraft.Copy();
            Assert.AreEqual(militaryAircraft.Weight, copy.Weight);
            Assert.AreEqual(militaryAircraft.Speed, copy.Speed);
            Assert.AreEqual(militaryAircraft.High, copy.High);
            Assert.AreEqual(militaryAircraft.Length, copy.Length);
            Assert.AreEqual(militaryAircraft.Width, copy.Width);
            Assert.AreEqual(militaryAircraft.CapacityIfPeople, copy.CapacityIfPeople);
            Assert.AreEqual(militaryAircraft.IsAtack, copy.IsAtack);
            Assert.AreEqual(militaryAircraft.ToString(), copy.ToString());
        }
    }
}
