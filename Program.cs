﻿using otus.pro.prototype.Entities;
using System;

namespace otus.pro.prototype
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("What's up!");

            var aircraft1 = new Aircraft(50, 850, 15, 80, 55, 200);
            Print("aircraft_1", aircraft1);
            var clone1 = aircraft1.Clone();
            Print("Clone Clone_1", clone1);


            var militaryAircraft = new MilitaryAircraft(50, 850, 15, 80, 55, 200, true);
            Print("militaryAircraft_1", militaryAircraft);
            var copy2 = militaryAircraft.Copy();
            Print("Copy militaryAircraft_2", copy2);
        }
        private static void Print(string name, object obj)
        {
            Console.WriteLine(name);
            Console.WriteLine(obj.ToString());
            Console.WriteLine();
        }
    }
}
