﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using otus.pro.prototype.Entities;

namespace otus.pro.prototype.test
{
    [TestClass]
    public class AircraftTest
    {
        [TestMethod]
        public void TestClone()
        {
            var aircraft = new Aircraft(50, 850, 15, 80, 55, 200);
            var clone = (Aircraft)aircraft.Clone();
            Assert.AreEqual(aircraft.Weight, clone.Weight);
            Assert.AreEqual(aircraft.Speed, clone.Speed);
            Assert.AreEqual(aircraft.High, clone.High);
            Assert.AreEqual(aircraft.Length, clone.Length);
            Assert.AreEqual(aircraft.Width, clone.Width);
            Assert.AreEqual(aircraft.CapacityIfPeople, clone.CapacityIfPeople);
            Assert.AreEqual(aircraft.ToString(), clone.ToString());
        }

        [TestMethod]
        public void TestCopy()
        {
            var aircraft = new Aircraft(50, 850, 15, 80, 55, 200);
            var copy = aircraft.Copy();
            Assert.AreEqual(aircraft.Weight, copy.Weight);
            Assert.AreEqual(aircraft.Speed, copy.Speed);
            Assert.AreEqual(aircraft.High, copy.High);
            Assert.AreEqual(aircraft.Length, copy.Length);
            Assert.AreEqual(aircraft.Width, copy.Width);
            Assert.AreEqual(aircraft.CapacityIfPeople, copy.CapacityIfPeople);
            Assert.AreEqual(aircraft.ToString(), copy.ToString());
        }
    }
}
