﻿using System;
using System.Collections.Generic;
using System.Text;

namespace otus.pro.prototype.Entities
{
    public class Transport : IMyCloneable<Transport>, ICloneable
    {
        public double Weight { get; set; }

        public int Speed { get; set; }

        public double High { get; set; }

        public double Length { get; set; }

        public double Width { get; set; }

        public Transport()
        {

        }

        public Transport(double weight, int speed, double high, double length, double width)
        {
            Weight = weight;
            Speed = speed;
            High = high;
            Length = length;
            Width = width;
        }


        public object Clone()
        {
            return Copy();
        }

        public Transport Copy()
        {
            return new Transport(Weight, Speed, High, Length, Width);
        }

        public override string ToString()
        {
            return $"Weight:\t{Weight},\n" +
                $"Speed:\t{Speed},\n" +
                $"High:\t{High},\n" +
                $"Length:\t{Length},\n" +
                $"Width:\t{Width}.";
        }

    }
}
